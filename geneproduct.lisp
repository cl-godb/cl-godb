;;; -*- Mode: Lisp -*-
(in-package "GODB")

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require "sql")
  (require "odbc"))

#.(sql:enable-sql-reader-syntax)


(defclass gene-product ()
  ((id :accessor gene-product-id :initarg :gene-product-id)
   (gene-symbol :accessor gene-symbol :initarg :gene-symbol)
   (dbxref-id :accessor dbxref-id :initarg :dbxref-id)
   (species-id :accessor species-id :initarg :species-id)
   (full-name :accessor full-name :initarg :full-name)
   (species-common-name :accessor species-common-name :initarg :species-common-name)
   )
  (:documentation "The Gene Product  Class.
This class is a representation of the gene product table in the go database, with the addition
of a slot for the species common name."))


(defgeneric gene-prod-go-accessions (gh gene-symbol
                                        &key
                                        species
                                        dbxref
                                        &allow-other-keys)
  (:documentation "Gets the accession numbers associatied with a gene product using its symbol.
        arguments: go handle, gene symbol
        usage: (godb:gene-prod-go-accessions mygohandle ""WNT2"")
        returns: accession number"))

(defgeneric go-accession-gene-products (gh go-acc
                                           &key
                                           species
                                           dbxref
                                           &allow-other-keys)
  (:documentation "Gets the gene products associated with an accession number.
        arguments: go handle, accession number
        usage: (godb:go-accession-gene-product mygohandle ""GO:0000126"")
        returns: List of objects of type gene-product
        notes: This function also adds the gene products to the hash table, indexed
               by its symbol if its not already there."))

(defgeneric term-gene-products (gh go-term
                                  &key
                                  species
                                  dbxref
                                  &allow-other-keys)
  (:documentation "Gets the gene products associated with a term.
        arguments: go handle, go term
        usage: (godb:term-gene-products mygohandle (godb:get-term mygohandle 156))
        returns: List of objects of type gene-product
        notes: Takes object of type term and returns a list of type gene product
               of the products associated with the input term.
               If the gene products already exist in the hash table, they are returned
               otherwise they are indexed by gene symbol and then returned."))

(defgeneric synonym-list (gh gene-symbol
                             &key
                             species
                             dbxref
                             &allow-other-keys)
  (:documentation "Finds product synonyms for the input gene symbol.
        arguments: go handle, gene symbol
        usage: (godb:synonym-list gh ""WNT2"")
        returns: List of synonyms"))

(defgeneric species-db (gh gene-symbol
                           &key
                           species
                           dbxref
                           &allow-other-keys)
  (:documentation "Gets the name of the species database for the given gene symbol.
        arguments: go handle, gene symbol
        usage: (godb:species-db mygohandle ""WNT2"")
        returns: Species database name"))

(defgeneric term-type (gh gene-symbol
                          &key
                          species
                          dbxref
                          &allow-other-keys)
  (:documentation "Returns what type of term is associated with the input gene symbol.
        arguments: go handle, gene symbol
        usage: (godb:term-type mygohandle ""WNT2"")
        returns: type of term  "))

(defgeneric is-gene-product-symbol-indexed-p (gh gene-symbol)
  (:documentation "Checks if a gene symbol is present in the hash table.
         arguments: go handle, gene symbol
        usage: (godb:is-gene-product-symbol-indexed-p mygohandle ""WNT2"")
        returns: Reference to object of gene product
        notes: If the given symbol is found in the index, a reference to its
                object is returned, otherwise the function returns NIL."))

(defgeneric index-gene-product-by-symbol (gh new-product gene-symbol)
  (:documentation "Indexes a gene product in the hash table, by its symbol.
        arguments: go handle, gene product, gene symbol
        usage: (godb:index-gene-product-by-symbol mygohandle mygeneproduct ""WNT2"")
        returns: -
        notes: This adds an object of type gene product to gene product symbol hash
               and indexes it by symbol.  This is normally only called by other functions"))


(defun create-product (id gene-symbol dbxref-id species-id full-name species-common-name)
"Makes a new object of type gene product given the input for its slots"
  (make-instance 'gene-product
                 :gene-product-id id
                 :gene-symbol gene-symbol
                 :dbxref-id dbxref-id
                 :species-id species-id
                 :full-name full-name
                 :species-common-name species-common-name
                 ))

(defmethod gene-prod-go-accessions ((gh go-handle) gene-symbol &key species dbxref &allow-other-keys) 
  (let ((accessions (sql:select [term.acc] 
                                :from '([gene_product] [association] [term])
                                :where [and [= gene-symbol [gene_product.symbol]]
                                [= [gene_product.id] [association.gene_product_id]]
                                [= [association.term_id] [term.id]]]
                                :flatp t)))
    (mapcar (lambda (acc) (intern acc "GODB")) accessions)
    ))


(defmethod go-accession-gene-products ((gh go-handle) (go-acc symbol) &key species dbxref &allow-other-keys)
  (go-accession-gene-products gh (string go-acc))
  )


(defmethod go-accession-gene-products ((gh go-handle) (go-acc string) &key species dbxref &allow-other-keys)
  (let ((gene-products (sql:select [gene_product.id] [gene_product.symbol] [gene_product.dbxref_id]
                                   [gene_product.species_id] [gene_product.full_name]
                                   :from '([gene_product] [term] [association])
                                   :where [and [= go-acc [term.acc]]
                                   [= [term.id] [association.term_id]]
                                   [= [association.gene_product_id] [gene_product.id]]]
                                   :distinct t)))
    (when gene-products
      (let ((product-list nil))
        (dolist (x gene-products)
          (let ((index (is-gene-product-symbol-indexed-p gh (second x))))
            (if index
                (push index product-list)
                (let ((new-product (create-product (first x) (second x) (third x) (fourth x) (fifth x) (sixth x))))
                  (index-gene-product-by-symbol gh new-product (gene-symbol new-product))
                  (push new-product product-list)))))
        product-list
        ))))

(defmethod term-gene-products ((gh go-handle) (go-term term) &key species dbxref &allow-other-keys)
  (let* ((acc (string (accession go-term)))
         (gene-products (sql:select [gene_product.id] [gene_product.symbol] [gene_product.dbxref_id]
                                    [gene_product.species_id] [gene_product.full_name]
                                    :from '([gene_product] [term] [association])
                                    :where [and [= acc [term.acc]]
                                    [= [term.id] [association.term_id]]
                                    [= [association.gene_product_id] [gene_product.id]]]
                                    :distinct t)))
    (when gene-products
      (let ((product-list nil))
        (dolist (x gene-products)
          (let ((index (is-gene-product-symbol-indexed-p gh (second x))))
            (if index
                (push index product-list)
                (let ((new-product (create-product (first x) (second x) (third x) (fourth x) (fifth x) (sixth x))))
                  (index-gene-product-by-symbol gh new-product (gene-symbol new-product))
                  (push new-product product-list)))))
        product-list
        ))))

(defmethod synonym-list ((gh go-handle) gene-symbol &key species dbxref &allow-other-keys)
  (let ((synonyms (sql:select [gene_product_synonym.product_synonym] 
                              :from '([gene_product_synonym] [gene_product])
                              :where [and [= [gene_product_synonym.gene_product_id] [gene_product.id]]
                              [= gene-symbol [gene_product.symbol]]]
                              :flatp t)))
    synonyms
    ))

(defmethod species-db ((gh go-handle) gene-symbol &key species dbxref &allow-other-keys)
  (let ((speciesdb (sql:select [gene_product_count.speciesdbname]
                               :from '([gene_product] [association] [gene_product_count])
                               :where [and [= gene-symbol [gene_product.symbol]]
                               [= [gene_product.id] [association.gene_product_id]]
                               [= [association.term_id] [gene_product_count.term_id]]]
                               :flatp t)))
    speciesdb
    ))

(defmethod term-type ((gh go-handle) gene-symbol &key species dbxref &allow-other-keys)
  (let ((term-type (sql:select [term.term_type]
                               :from '([gene_product] [association] [term])
                               :where [and [= gene-symbol [gene_product.symbol]] 
                               [= [gene_product.id] [association.gene_product_id]]
                               [= [association.term_id] [term.id]]]
                               :flatp t)))
    term-type
    ))

(defmethod is-gene-product-symbol-indexed-p ((gh go-handle) gene-symbol)
  (multiple-value-bind (value found) 
      (gethash gene-symbol (gene-product-symbol-index gh))
    (when found value)
    ))


(defmethod index-gene-product-by-symbol ((gh go-handle) new-product gene-symbol)
  (setf (gethash gene-symbol (gene-product-symbol-index gh)) new-product)
)

#.(sql:disable-sql-reader-syntax)

;;; end of file -- gogeneproduct.lisp --
