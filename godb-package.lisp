;;; -*- Mode: Lisp -*-

(defpackage "CL-GODB" (:use "CL")
  (:nicknames "GODB")

  (:export
   "GO-HANDLE"
   "GO-HANDLE-P"
   "MAKE-GODB-HANDLE"
   "*CURRENT-GODB-HANDLE*")
  (:export "TERM")
  (:export 
   "SQLCON"
   "CONNECT")
  (:export 
   "GET-TERM"
   "GET-TERM-BY-ACC"
   "GET-TERM-BY-NAME"
   "GET-NAME"             
   "GET-ROOT-TERMS")
  (:export "HAS-SYNONYM-P")
  (:export
   "INDEX-TERM-BY-ID"
   "INDEX-TERM-BY-ACCESSION"
   "INDEX-TERM-BY-NAME"
   "IS-TERM-ID-INDEXED-P"
   "IS-TERM-ACCESSION-INDEXED-P"
   "IS-TERM-NAME-INDEXED-P")

  ;; go association 
  (:export 
   "ASSOCIATION"
   "CREATE-ASSOCIATION"
   "ASSOCIATIONS-BY-GENE-PROD"
   "ASSOCIATION-LIST"
   "IS-ASSOCIATION-ACCESSION-INDEXED-P"
   "IS-ASSOCIATION-GENE-PROD-INDEXED-P"
   "INDEX-ASSOCIATION-BY-ACCESSION"
   "INDEX-ASSOCIATION-BY-GENE-PROD")

  ;; go relationship
  (:export "RELATIONSHIP")

  ;; go graph
  (:export "COUNT-ALL-NODES")
  (:export "COUNT-NODES")
  (:export 
   "N-CHILDREN"
   "N-PARENTS")
  (:export 
   "GET-CHILD-RELATIONSHIPS"
   "GET-PARENT-RELATIONSHIPS"
   "GET-RELATIONSHIPS"
   "GET-CHILD-TERMS"
   "GET-PARENT-TERMS")
  (:export "IS-LEAF-NODE")
  (:export "FIND-ROOTS")
  (:export "GET-LEAF-NODES") 

  ;; go gene product
  (:export 
   "GENE-PRODUCT"
   "GENE-PROD-GO-ACCESSIONS"
   "GO-ACCESSION-GENE-PRODUCTS"
   "TERM-GENE-PRODUCTS"
   "SYNONYM-LIST"
   "SPECIES-DB"
   "TERM-TYPE"
   "IS-GENE-PRODUCT-SYMBOL-INDEXED-P"
   "INDEX-GENE-PRODUCT-BY-SYMBOL")
  (:export
   "VIEW-TREE"
   "VIEW-CL-GODB")
  )



;;; end of file -- godb-package.lisp --
