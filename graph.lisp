;;; -*- Mode: Lisp -*-

;;; graph.lisp --
;;; Functions related to the manipulation of the GO taxonomies.

(in-package "GODB")

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require "sql")
  (require "odbc"))


#.(sql:enable-sql-reader-syntax)

#|
(defvar +rel-is-a+ 2)
(defvar +rel-part-of+ 3)
|#

(defgeneric n-children (gh go-acc)
  (:documentation "Counts the number of children the term has.
        arguments: go handle, go-id
        usage: (godb:n-children mygohandle ""GO:0000126"")
        returns: int number of children that term has"))

(defgeneric n-parents (gh go-acc)
  (:documentation "Counts the number of children the term has.
        arguments: go handle, accession number
        usage: (godb:get-child-relationships  mygohandle ""GO:0000126"")
        returns: List of objects of type relationship
        notes:"))

(defgeneric get-child-relationships (gh go-acc)
  (:documentation "Gets children for the input accession and returns an object of type
relationship for each child.
        arguments: go handle, accession number
        usage: (godb:get-child-relationships  mygohandle ""GO:0000126"")
        returns: List of objects of type relationship"))

(defgeneric get-parent-relationships (gh go-acc)
  (:documentation "Gets parents for the input accession and returns an object of type 
relationship for each parent.
        arguments: go handle, accession number
        usage: (godb:get-parent-relationships  mygohandle ""GO:0000126"")
        returns: list of objects of type relationship"))

(defgeneric get-relationships (gh go-acc)
  (:documentation "This function calls get-child-relationships and get-parent-relationships
and then returns all relations.
        arguments: go handle, accession number
        usage: (godb:get-relationships  mygohandle ""GO:0000126"")
        returns: list of objects of type relationship"))

(defgeneric get-child-terms (gh go-acc)
  (:documentation "Gets children for the input accession using is-a hierarchy and returns them as objects of type term.
        arguments: go handle, accession
        usage: (godb:get-child-terms mygohandle ""GO:0000124"")
        returns: list of objects of type term
        notes: This function finds the ids for all children of the input accession
               then it checks if they are already in the hash table.  If they are not,
               they are then added.  In both cases, the children slot of the parent's
               term object is filled with a list of references to the childrens' term
               objects."))


(defgeneric get-parent-terms (gh go-acc)
  (:documentation "Gets parents for the input accession using is-a hierarchy and returns them as objects of type term.
        arguments: go handle, accession
        usage: (godb:get-parent-terms mygohandle ""GO:0000124"")
        returns: list of objects of type term
        notes: This function finds the ids for all parents of the input accession
               then it checks if they are already in the hash table.  If they are not,
               they are then added.  In both cases, the is-a slot of the child's
               term object is filled with a list of references to the parents' term
               objects."))

(defgeneric get-component-terms (gh go-acc)
  (:documentation "Gets children for the input accession using part-of hierarchy and returns them as objects of type term.
        arguments: go handle, accession
        usage: (godb:get-component-terms mygohandle ""GO:0000124"")
        returns: list of objects of type term
        notes: This function finds the ids for all children of the input accession
               then it checks if they are already in the hash table.  If they are not,
               they are then added.  In both cases, the components slot of the parent's
               term object is filled with a list of references to the childrens' term
               objects."))


(defgeneric get-container-terms (gh go-acc)
  (:documentation "Gets parents for the input accession using part-of hierarchy and returns them as objects of type term.
        arguments: go handle, accession
        usage: (godb:get-container-terms mygohandle ""GO:0000124"")
        returns: list of objects of type term
        notes: This function finds the ids for all parents of the input accession
               then it checks if they are already in the hash table.  If they are not,
               they are then added.  In both cases, the part-of slot of the child's
               term object is filled with a list of references to the parents' term
               objects."))


(defgeneric get-all-descendants (gh go-acc)
  (:documentation "Gets children for the input accession using part-of hierarchy AND is-a hierarchy.
        arguments: go handle, accession
        usage: (godb:get-all-descendants mygohandle ""GO:0000124"")
        returns: list of objects of type term"))


(defgeneric get-all-ancestors (gh go-acc)
  (:documentation "Gets parents for the input accession using part-of hierarchy AND is-a hierarchy
        arguments: go handle, accession
        usage: (godb:get-all-ancestors mygohandle ""GO:0000124"")
        returns: list of objects of type term"))


(defgeneric is-leaf-node (gh go-acc)
  (:documentation "Checks whether the given accession is a leaf node or not.
        arguments: go handle, go-id
        usage: (godb:is-leaf-node gh go-id)
        returns: T if it's a leaf node, NIL if it's not"))


(defgeneric ancestor-p (gh term1 term2)
  (:documentation "Tests whether term2 is an ancestor of term1
       arguments: go handle, object of type term, object of type term
       usage: (godb:ancestor-p gh term1 term2)
       returns: T or NIL"))

(defgeneric descendant-p (gh term1 term2)
  (:documentation "Tests whether term1 is an descendant of term2
       arguments: go handle, object of type term, object of type term
       usage: (godb:descendant-p gh term1 term2)
       returns: T or NIL"))


;;;Methods

(defmethod n-children ((gh go-handle) (go-term term))

  (n-children gh (accession go-term))
  )

(defmethod n-children ((gh go-handle) (go-acc symbol))
  (n-children gh (string go-acc))
  )


(defmethod n-children ((gh go-handle) (go-acc string))
  ;; counts children in is-a relationship
  (let ((myterm (godb::get-term-by-acc gh go-acc)))
    (when myterm
      (if (children myterm)
          (list-length (children myterm))
          (let ((num (sql:select [count [*]] 
                                 :from '([term "PARENT"] [term2term] [term "CHILD"]) 
                                 :where [and [= ["PARENT" acc] go-acc] 
                                 [= ["PARENT" id] [term2term.term1_id]]
                                 [= ["CHILD" id] [term2term.term2_id]]
                                 [= +rel-is-a+ [term2term.relationship_type_id]]])))
            num)
          ))))


(defmethod n-parents ((gh go-handle) (go-acc symbol))
  (n-parents gh (string go-acc))
  )

(defmethod n-parents ((gh go-handle) (go-acc string))
  ;; counts parents in is-a relationship
  (let ((myterm (godb::get-term-by-acc gh go-acc)))
    (when myterm
      (if (is-a myterm)
          (list-length (is-a myterm))
          (let ((num (sql:select [count [*]] 
                                 :from '([term "PARENT"] [term2term] [term "CHILD"]) 
                                 :where [and [= ["CHILD" acc] go-acc] 
                                 [= ["PARENT" id] [term2term.term1_id]]
                                 [= ["CHILD" id] [term2term.term2_id]]
                                 [= +rel-is-a+ [term2term.relationship_type_id]]])))
            num)
          ))))



(defmethod get-child-relationships ((gh go-handle) (go-acc symbol))
  (get-child-relationships gh (string go-acc))
  )


(defmethod get-child-relationships ((gh go-handle) (go-acc string))
  (let ((myterm (gethash (intern go-acc "GODB") (term-accession-index gh))))
    (if myterm
        (let ((kids nil))
          (dolist (x (is-a myterm))
            (if (gethash x (term-id-index gh))
                (let ((kid-acc (accession (gethash x (term-id-index gh)))))
                  (let ((kid-acc (accession (godb:get-term gh x))))
                  
                    (let ((new-relation (make-instance 'relationship
                                                       :acc1 go-acc
                                                       :acc2 kid-acc
                                                       :type +rel-is-a+)))
                      (push new-relation kids)))))kids))
           
        (let ((children (sql:select [parent.acc] [child.acc] [term2term.relationship_type_id] 
                                    :from '([term "PARENT"] [term2term] [term "CHILD"])
                                    :where [and [= ["PARENT" acc] go-acc] 
                                    [= ["PARENT" id] [term2term.term1_id]] 
                                    [= ["CHILD" id][term2term.term2_id]]
                                    [=  +rel-is-a+ [term2term.relationship_type_id]]])))
          (when children
            (let ((kids nil))
              (dolist (x children)
                (let ((new-relation (make-instance 'relationship
                                                   :acc1 (intern (first x) "GODB")
                                                   :acc2 (intern (nth 1 x) "GODB")
                                                   :type (nth 2 x))))
                  (push new-relation kids)))
              kids))))))


(defmethod get-parent-relationships ((gh go-handle) (go-acc symbol))
  (get-parent-relationships gh (string go-acc))
  )

(defmethod get-parent-relationships ((gh go-handle) (go-acc string))
  (let ((myterm (gethash (intern go-acc "GODB") (term-accession-index gh))))
    (if myterm
        (let ((parents nil))
          (dolist (x (part-of myterm))
            (if (gethash x (term-id-index gh))
                (let ((par-acc (accession (gethash x (term-id-index gh)))))
                  (let ((par-acc (accession (godb:get-term gh x))))
                  
                    (let ((new-relation (make-instance 'relationship
                                                       :acc1 (intern go-acc "GODB")
                                                       :acc2 par-acc
                                                       :type +rel-part-of+)))
                      (push new-relation parents))))) parents))


        (let ((parents (sql:select [parent.acc] [child.acc] [term2term.relationship_type_id] 
                                   :from '([term "PARENT"] [term2term] [term "CHILD"]) 
                                   :where [and [= ["CHILD" acc] go-acc] 
                                   [= ["CHILD" id] [term2term.term2_id]]
                                   [= ["PARENT" id] [term2term.term1_id]]
                                   [= +rel-is-a+ [term2term.relationship_type_id]]])))
          (when parents
            (let ((parlist nil))
              (dolist (x parents)
                (let ((new-relation (make-instance 'relationship
                                                   :acc1 (intern (first x) "GODB")
                                                   :acc2 (intern (nth 1 x) "GODB")
                                                   :type (nth 2 x))))
                  (push new-relation parlist)))
              parlist)))))) 


(defmethod get-relationships ((gh go-handle) (go-acc symbol))
  (get-relationships gh (string go-acc))
)

(defmethod get-relationships ((gh go-handle) (go-acc string))
  (let* ((parents (get-parent-relationships gh go-acc))
         (children (get-child-relationships gh go-acc)))
    (format t "Parents: ~A ~%Children: ~A" parents children)
    ))


(defmethod get-child-terms ((gh go-handle) (go-term term))
  (get-child-terms gh (accession go-term)))


(defmethod get-child-terms ((gh go-handle) (go-acc symbol))
  (get-child-terms gh (string go-acc)))


(defmethod get-child-terms ((gh go-handle) (go-acc string))
  (let ((myterm (godb:get-term-by-acc gh go-acc)))
    (when myterm
        (if (children myterm)
            (children myterm)
            (let* ((kids nil)
                   (children-list (sql:select ["CHILD" id] 
                                              :from '([term "PARENT"] [term2term] [term "CHILD"]) 
                                              :where [and [= ["PARENT" acc] go-acc] 
                                              [= ["PARENT" id] [term2term.term1_id]]
                                              [= ["CHILD" id] [term2term.term2_id]]
                                              [= +rel-is-a+ [term2term.relationship_type_id]]]
                                              :flatp t)))
              (dolist (x children-list)
                (if (gethash x (term-id-index gh))
                    (push (gethash x (term-id-index gh)) kids)
                    (let ((rel (godb:get-term gh x)))
                      (push rel kids))))
              (setf (children myterm) kids)
              kids)))))


(defmethod get-parent-terms ((gh go-handle) (go-term term))
  (get-parent-terms gh (accession go-term)))


(defmethod get-parent-terms ((gh go-handle) (go-acc symbol))
  (get-parent-terms gh (string go-acc)))


(defmethod get-parent-terms ((gh go-handle) (go-acc string))
  (let ((myterm (godb:get-term-by-acc gh go-acc)))
    (when myterm
        (if (is-a myterm)
            (is-a myterm)
            (let* ((parents nil)
                   (parent-list (sql:select ["PARENT" id] 
                                              :from '([term "PARENT"] [term2term] [term "CHILD"]) 
                                              :where [and [= ["CHILD" acc] go-acc] 
                                              [= ["PARENT" id] [term2term.term1_id]]
                                              [= ["CHILD" id] [term2term.term2_id]]
                                              [= +rel-is-a+ [term2term.relationship_type_id]]]
                                              :flatp t)))
              (dolist (x parent-list)
                (if (gethash x (term-id-index gh))
                    (push (gethash x (term-id-index gh)) parents)
                    (let ((rel (godb:get-term gh x)))
                      (push rel parents))))
              (setf (is-a myterm) parents)
              parents)))))

(defmethod get-container-terms ((gh go-handle) (go-term term))
  (get-container-terms gh (accession go-term)))


(defmethod get-container-terms ((gh go-handle) (go-acc symbol))
  (get-container-terms gh (string go-acc)))


(defmethod get-container-terms ((gh go-handle) (go-acc string))
  (let ((myterm (godb:get-term-by-acc gh go-acc)))
    (when myterm
      (if (part-of myterm)
          (part-of myterm)
          (let* ((parents nil)
                 (parent-list (sql:select ["PARENT" id] 
                                          :from '([term  "PARENT"] [term2term] [term  "CHILD"]) 
                                          :where [and [= ["CHILD" acc] go-acc] 
                                          [= ["PARENT" id] [term2term.term1_id]]
                                          [= ["CHILD" id] [term2term.term2_id]]
                                          [= +rel-part-of+ [term2term.relationship_type_id]]]
                                          :flatp t)))
            (dolist (x parent-list)
              (if (gethash x (term-id-index gh))
                  (push (gethash x (term-id-index gh)) parents)
                  (let ((rel (godb:get-term gh x)))
                    (push rel parents))))
            (setf (part-of myterm) parents)
            parents)))))

(defmethod get-component-terms ((gh go-handle) (go-term term))
  (get-component-terms gh (accession go-term)))


(defmethod get-component-terms ((gh go-handle) (go-acc symbol))
  (get-component-terms gh (string go-acc)))


(defmethod get-component-terms ((gh go-handle) (go-acc string))
  (let ((myterm (godb:get-term-by-acc gh go-acc)))
    (when myterm
      (if (components myterm)
          (components myterm)
          (let* ((p-children nil) 
                 (p-list (sql:select ["CHILD" id] 
                                     :from '([term "PARENT"] [term2term] [term "CHILD"]) 
                                     :where [and [= ["PARENT" acc] go-acc] 
                                     [= ["PARENT" id] [term2term.term1_id]]
                                     [= ["CHILD" id] [term2term.term2_id]]
                                     [= +rel-part-of+ [term2term.relationship_type_id]]]
                                     :flatp t)))
            (dolist (x p-list)
              (if (gethash x (term-id-index gh))
                  (push (gethash x (term-id-index gh)) p-children)
                  (let ((rel (godb:get-term gh x)))
                    (push rel p-children))))
            (setf (components myterm) p-children)
            p-children)))))



(defmethod get-all-descendants ((gh go-handle) (go-term term))
  (get-all-descendants gh (accession go-term)))

(defmethod get-all-descendants ((gh go-handle) (go-acc symbol))
  (get-all-descendants gh (string go-acc)))

(defmethod get-all-descendants ((gh go-handle) (go-acc string))
  (append (get-component-terms gh go-acc)
          (get-child-terms gh go-acc)))


(defmethod get-all-ancestors ((gh go-handle) (go-term term))
  (get-all-ancestors gh (accession go-term)))

(defmethod get-all-ancestors ((gh go-handle) (go-acc symbol))
  (get-all-ancestors gh (string go-acc)))

(defmethod get-all-ancestors ((gh go-handle) (go-acc string))
  (append (get-parent-terms gh go-acc)
          (get-container-terms gh go-acc)))




(defmethod is-leaf-node ((gh go-handle) go-acc)
  (if (get-child-terms gh go-acc) 
      NIL 
      T))



(defmethod ancestor-p ((gh go-handle) (term1 term) (term2 term))
  ;; Checks if term 2 is an ancestor of term 1
  ;; if is-a slot is empty, need to fill it
  (let ((parent-list (get-parent-terms gh term2))))

  (if (eq term1 term2)
      t
      (some #'(lambda (p) (ancestor-p gh term1 p)) (is-a term2))))

(defmethod descendant-p ((gh go-handle) (term1 term) (term2 term))
  ;; Checks if term 1 is a descendant of term 2
  ;; need to add check. If children slot is empty, fill it
  (let ((child-list (godb:get-child-terms gh term2))))

  (if (eq (term-id term1) (term-id term2))
      t
      (some #'(lambda (p) (descendant-p gh term1 p)) (children term2))))



(defun choose-leaves-from-set (gh term-set)
"Given a list of terms and a go handle, the function returns the terms
which are not parents of any of the others in the list."
  (declare (type list term-set))
  (let* ((leaves-set (make-hash-table))
         (leaf-list nil))
    ;; First fill the hash-table
    (dolist (term term-set)
      (setf (gethash (term-id term) leaves-set) term))

    (dolist (term term-set)
      (loop for l being the hash-values in leaves-set
            using (hash-key l-id)
            when (and (not (eq l term)) (descendant-p gh term l))
            do (remhash l-id leaves-set)))
    (loop for l being the hash-values in leaves-set
          do (push l leaf-list))
    leaf-list))


(defun count-all-nodes ()
  "Returns the number of nodes."
  (sql:select [count [*]] :from [|term|]))



#.(sql:disable-sql-reader-syntax)

;;; end of file -- gograph.lisp --
