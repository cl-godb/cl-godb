(in-package "GODB")

(capi:define-interface cl-godb ()
  ()
  (:panes
   (tree-viewer-2
    go-tree-viewer)

   (selected-term-id
    capi:display-pane
    :text "Selected Term id"
    :visible-min-height '(:character 1)
    :visible-min-width '(:character 40)
    :visible-max-height '(:character 1)
    :visible-max-width nil
    :accessor selected-term-id-display)

   (selected-term-name
    capi:display-pane
    :text "Selected Term name"
    :visible-min-height '(:character 1)
    :visible-min-width '(:character 40)
    :horizontal-scroll t
    :visible-max-height '(:character 1)
    :visible-max-width nil
    :accessor selected-term-name-display)

   (selected-term-accession
    capi:display-pane
    :text "Selected Term accession"
    :visible-min-height '(:character 1)
    :visible-min-width '(:character 40)

    :visible-max-height '(:character 1)
    :visible-max-width nil
    :accessor selected-term-accession-display)

   (selected-term-gene-symbols
    capi:list-panel
    :text "Selected Term gene symbols"
    :visible-min-height '(:character 1)
    :visible-min-width '(:character 40)
    :horizontal-scroll t

    :visible-max-height '(:character 5)
    :visible-max-width nil

    :print-function 'godb::gene-symbol
    :accessor selected-term-gene-symbols-list
    :callback-type :interface-item
    :selection-callback 'display-selected-symbol)
    
   (selected-symbol-synonyms
    capi:list-panel
    :text "Gene synonyms"
    :visible-min-height '(:character 1)
    :visible-min-width '(:character 40)
    :visible-max-height '(:character 4)
    :visible-max-width nil
    :accessor selected-symbol-synonym-list
    )

   (selected-symbol-gene-name
    capi:display-pane
    :text "Gene Full Name"
    :visible-min-height '(:character 1)
    :visible-min-width '(:character 40)
    :visible-max-height '(:character 1)
    :visible-max-width nil
    :accessor selected-symbol-gene-name-display)


   (assoc-tree 
    capi:tree-view
    :accessor assoc-tree
    :children-function (lambda (entry) (get-tree-view-ancestors entry))
    :print-function (lambda (entry) (term-name (tree-view-info-entry-term entry)))
    :image-lists (list :normal *my-image-list*)
    :image-function #'(lambda (x) (tree-view-info-entry-up-relationship x))
    :visible-min-width 200
    :visible-max-width nil
    :visible-min-height 200
    :visible-max-height nil
    :retain-expanded-nodes t
    :callback-type :interface-item
    :horizontal-scroll t
    :vertical-scroll t
    )
   )

  (:layouts
   (row-layout-1
    capi:row-layout
    '(tree-viewer-2 :divider grid-layout-1))
   (grid-layout-1
    capi:grid-layout
    '("" "Term data"
         "Term id:" selected-term-id
         "Accession:" selected-term-accession
         "Name:" selected-term-name
         "Gene Symbols:" selected-term-gene-symbols
         "" ""
         "" "Gene symbol data"
         "Full name" selected-symbol-gene-name
         "Synonyms" selected-symbol-synonyms
         "Associated Terms" assoc-tree
         )
    :columns 2
    ;; :y-uniform-size-p nil
    ;; :visible-min-width 460
    ))
  (:default-initargs
   :best-height 500
   :visible-min-width 700
   :default-x 300
   :default-y 500
   :layout 'row-layout-1
   :title "CL-GODB"))


(defun display-selected-term (tree-viewer-interface entry)
  (let ((term (tree-view-info-entry-term entry)))
    (setf (capi:display-pane-text (selected-term-id-display (capi:top-level-interface tree-viewer-interface)))
          (format nil "~A"(term-id term)))
    (setf (capi:display-pane-text (selected-term-name-display (capi:top-level-interface tree-viewer-interface)))
          (format nil "~A"(term-name term)))
    (setf (capi:display-pane-text (selected-term-accession-display (capi:top-level-interface tree-viewer-interface)))
          (format nil "~A"(accession term)))
    (let ((gp (godb::go-accession-gene-products godb::*current-godb-handle* (accession term))))
      (setf (capi:collection-items (selected-term-gene-symbols-list (capi:top-level-interface tree-viewer-interface)))
            gp)     
      ;;reset gene symbol data
      (setf (capi:display-pane-text (selected-symbol-gene-name-display (capi:top-level-interface tree-viewer-interface)))
            (format nil "Gene Full Name"))
      (setf (capi:collection-items (selected-symbol-synonym-list (capi:top-level-interface tree-viewer-interface)))
            '("Gene synonyms"))
      (setf (capi:tree-view-roots (assoc-tree (capi:top-level-interface tree-viewer-interface)))
            '())
      )))


(defun display-selected-symbol (interface symbol)
  "The function called when a gene symbol is selected.
Displays full name, synonyms, and tree of associated terms."
  (setf (capi:display-pane-text (selected-symbol-gene-name-display (capi:top-level-interface interface)))
        (format nil "~A" (string-right-trim "" (full-name symbol))))
  (let* ((symbols (godb:synonym-list godb::*current-godb-handle* (gene-symbol symbol)))
         (symlist nil))
    (if symbols
        (dolist (x symbols)
          (push (string-right-trim "" x) symlist))
        (setf symlist '("None")))
    (setf (capi:collection-items (selected-symbol-synonym-list (capi:top-level-interface interface)))
          symlist))

  (let* ((associations (godb:associations-by-gene-prod godb::*current-godb-handle* (gene-product-id symbol)))
         (assoclist nil)
         (assoc-entries nil))
    (when associations
      (dolist (x associations)
        (push (godb::get-term-by-acc godb::*current-godb-handle* (assoc-acc x)) assoclist))
      (dolist (y (godb::choose-leaves-from-set godb::*current-godb-handle* assoclist))
        (push (make-tree-view-info-entry :term y
                                         :up-relationship 2) assoc-entries)))
    
    (setf (capi:tree-view-roots (assoc-tree (capi:top-level-interface interface)))
          assoc-entries)))


(defun view-cl-godb ()
  "Displays paned tree view, with Ontology tree, term information, and gene symbol 
information."
  (capi:display (make-instance 'cl-godb)))
