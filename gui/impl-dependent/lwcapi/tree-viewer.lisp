;;; -*- Mode: Lisp -*-

(in-package "GODB")


;;----------------------------------------------------------------------------
;; Create an image list for use by the tree-view
;;----------------------------------------------------------------------------

(defvar *my-image-list*
  (make-instance 'capi:image-list
		  :image-sets (list (capi:make-general-image-set
				     :id #.(gp:read-external-image (lispworks:current-pathname "tree.bmp"))
				     :image-count 2))
		  :image-width 16
		  :image-height 16))


;;----------------------------------------------------------------------------
;; Define the interface
;;----------------------------------------------------------------------------




(capi:define-interface tree-viewer ()
  ((go-handle :reader go-handle :initarg :godb-handle))
  (:panes
   (tree capi:tree-view
         :accessor tree

         :roots (list (godb::get-term-by-acc godb::*current-godb-handle* "GO:0008150") 
                      (godb:get-term-by-acc godb::*current-godb-handle* "GO:0005575")
                      (godb:get-term-by-acc godb::*current-godb-handle* "GO:0003674"))
         :children-function (lambda (term)
                              (godb::get-child-terms godb::*current-godb-handle* term))

         :print-function 'godb::term-name

         :image-lists (list :normal *my-image-list*)
         :image-function #'(lambda (x) 0)
         :min-width 350
         :min-height 500 
         :retain-expanded-nodes t
         :callback-type :interface-item
         :selection-callback #'(lambda (self item)
                                 (format t "~&Select item ~S data ~S ~%" item self))
         :action-callback 'display-selected-term
         :delete-item-callback #'(lambda (self item)
                                   (declare (ignore self))
                                   (format t "~&Delete item ~S~%" item))))
  (:layouts
   (default-layout
    capi:simple-layout
    '(tree)))
  (:default-initargs
   :title "CL-GODB Tree View"))



;;----------------------------------------------------------------------------
;; A simple test function
;;----------------------------------------------------------------------------

(defun view-tree ()
  (capi:display (make-instance 'tree-viewer)))
