;;; -*- Mode: Lisp -*-

(in-package "GODB")


;;----------------------------------------------------------------------------
;; Create an image list for use by the tree-view
;;----------------------------------------------------------------------------

(defvar *my-image-list*
  (make-instance 'capi:image-list
		  :image-sets (list (capi:make-general-image-set
				     :id #.(gp:read-external-image (lispworks:current-pathname "icons.bmp"))
				     :image-count 2))
		  :image-width 16
		  :image-height 16))


;;----------------------------------------------------------------------------
;; Define the interface
;;----------------------------------------------------------------------------



(defstruct tree-view-info-entry
  up-relationship
  term)


(defun get-tree-view-descendants (entry)
  (let ((term (tree-view-info-entry-term entry)))
    (godb::get-all-descendants godb::*current-godb-handle* term)

    (append (mapcar (lambda (term)
                      (make-tree-view-info-entry :term term
                                                 :up-relationship 0 ;:is-a
                                                 ))
                    (children term))
            (mapcar (lambda (term)
                      (make-tree-view-info-entry :term term
                                                 :up-relationship 1 ;:part-of
                                                 ))
                    (components term)))))


(defun get-tree-view-ancestors (entry)
  (let ((term (tree-view-info-entry-term entry)))
    (godb::get-all-ancestors godb::*current-godb-handle* term)

    (append (mapcar (lambda (term)
                      (make-tree-view-info-entry :term term
                                                 :up-relationship 0 ;:is-a
                                                 ))
                    (is-a term))
            (mapcar (lambda (term)
                      (make-tree-view-info-entry :term term
                                                 :up-relationship 1 ;:part-of
                                                 ))
                    (part-of term)))))


(defun make-roots()
  (let* ((roots nil))
    (push (make-tree-view-info-entry :term (godb::get-term-by-acc godb::*current-godb-handle* "GO:0003674")
                                     :up-relationship 2) roots)
    (push (make-tree-view-info-entry :term (godb:get-term-by-acc godb::*current-godb-handle* "GO:0005575")
                                     :up-relationship 2) roots)
    (push (make-tree-view-info-entry :term (godb:get-term-by-acc godb::*current-godb-handle* "GO:0008150")
                                     :up-relationship 2) roots)
    roots))



(defclass go-tree-viewer (capi:tree-view)
  ((go-handle :reader go-handle :initarg :godb-handle))
  (:default-initargs
   :roots (make-roots)
   :children-function (lambda (entry) (get-tree-view-descendants entry))

   :print-function (lambda (entry) (term-name (tree-view-info-entry-term entry)))

   :image-lists (list :normal *my-image-list*)
   :image-function #'(lambda (x) (tree-view-info-entry-up-relationship x))
   :min-width 350
   :min-height 500 
   :retain-expanded-nodes t
   :callback-type :interface-item
   :selection-callback 'display-selected-term 
   :action-callback #'(lambda (self item)
                        (format t "~&Select item ~S data ~S ~%" item self))
   :delete-item-callback #'(lambda (self item)
                             (declare (ignore self))
                             (format t "~&Delete item ~S~%" item))


   :title "CL-GODB Tree View"))




(defun view-tree ()
"Displays simple tree."
  (capi:display (make-instance 'tree-viewer)))
