CL-GODB

The Gene Ontology (GO), is a controlled vocabulary used to aid the 
description of gene and gene product attributes in a variety of 
organisms. There are three different ontologies that describe 
molecular functions, biological processes, and cellular components.

CL-GODB provides a new way to access and interact with the GO 
Database. As queries are made, the results are indexed locally. 
This indexing makes successive queries faster, requiring fewer 
database accesses. In addition to the library of functions, there 
is a graphical interface that allows the user to browse the database 
in tree form.

Requirements:

GO Database -- which requires MySQL.
Lispworks Enterprise Edition 4.2 or higher -- this restriction will
be removed soon by making sure that the MySQL connection works
on the freely available CL-SQL.


Last Update: 2005-08-01

Contact: marcoxa at cs.nyu.edu, sjk267 at nyu.edu

Copyright 2005 Marco Antoniotti, Samantha Kleinberg
