;;; -*- Mode: Lisp -*-

(in-package "GODB")

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require "sql")
  (require "odbc"))


(defclass relationship ()
  ((acc1 :accessor rel-acc1 ; this is parent/subject/acc1
         :accessor get-subject
         :initarg :subject)
   (acc2 :accessor rel-acc2 ; this is child/object/acc2
         :accessor get-object
         :initarg :object)
   (type :accessor rel-type ; relationship type/predicate
         :accessor get-type
         :initarg :type
         :type (member :is-a :part-of))
   )
  (:default-initargs :type :is-a)
  (:documentation "The Relationship Class.
This class is used to store accession numbers for parents/children 
along with the type of relationship they have.")
  )


;;; end of file -- relationship.lisp --
