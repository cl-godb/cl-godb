;;; -*- Mode: Lisp -*-


(in-package "GODB")

(defclass mesh-handle (o-handle)
  ((terms-by-ui :type hash-table
                :reader term-ui-index
                :initform (make-hash-table :test #'eq))
   (terms-by-name :type hash-table
                  :reader term-string-index
                  :initform (make-hash-table :test #'equal))
   (terms-by-tree-number :type hash-table
                         :reader term-tree-number-index
                         :initform (make-hash-table :test #'equal))
   ))


(defclass mesh-node (node)
  ((tree-number :accessor tree-number
            :initarg :tree-number)
   ))


(defclass mesh-term (mesh-node)
  ((term-ui :accessor term-ui 
            :initarg :term-ui)
   (string :accessor name
           :accessor mesh-string
           :initarg :mesh-string)
   ))

(defvar *current-mesh-handle* nil)

(defun make-mesh-handle ()
  (setf *current-mesh-handle* (make-instance 'mesh-handle)))

(defun initialize-mesh-session ()
  (make-mesh-handle)
  (make-mesh-nodes *current-mesh-handle*)
)



(defun read-lines (f)
  (with-open-file (s f :direction :input)
    (loop for line = (read-line s nil nil)
          while line
          collect line)))




(defun get-mesh-roots()
  (let ((roots nil))
    (push (gethash  "A01" (term-tree-number-index *current-mesh-handle*)) roots)
    roots)
  )


#|(defmethod make-term ((mh mesh-handle) (treenum string))
  (let* ((lines (read-lines "Z:/cl-godb/mtrees2005.bin"))
         (node))
    (dolist (x lines node)
      (let* ((split-line (split-by-semi-colon x)))
        (when (equal (second split-line) treenum)
          (setf node (make-instance 'mesh-term
                                    :mesh-string (first split-line)
                                    :tree-number (second split-line)))
          (index-term-by-string mh node (first split-line))
          (index-term-by-tree-number mh node (second split-line)))
        ))))|#


(defmethod make-mesh-nodes ((mh mesh-handle))
  (let* ((lines (read-lines "Z:/cl-godb/mtrees2005.bin"))
         (node-list nil))
    (dolist (x lines)
      (let* ((split-line (split-by-semi-colon x)))
        (push (make-instance 'mesh-term
                             :mesh-string (first split-line)
                             :tree-number (second split-line)) 
              node-list)))
    (dolist (y node-list)
      (index-term-by-string mh y (mesh-string y))
      (index-term-by-tree-number mh y (tree-number y))
      )
    ))

(defmethod child-nodes ((mh mesh-handle) (term mesh-term))
  ;;takes a term and fills its children slot.  Returns list of children's mesh-terms
  (let* ((lines (read-lines "Z:/cl-godb/mtrees2005.bin"))
         (children nil))
    (dolist (x lines)
      (let* ((split-line (split-by-semi-colon x)))
        (when (and (eq (length (second split-line)) (+ 4 (length (tree-number term))))
                   (search (tree-number term) (second split-line)))
          (push (make-instance 'mesh-term
                               :mesh-string (first split-line)
                               :tree-number (second split-line)) 
                children))))
    (setf (children term) children)
    children))


(defun split-by-semi-colon (string)
  "Returns a list of substrings of string
divided by ONE ; each."
  (loop for i = 0 then (1+ j)
        as j = (position #\; string :start i)
        collect (subseq string i j)
        while j))



(defmethod index-term-by-ui (mesh-handle new-term ui)
  (setf (gethash ui (term-ui-index mesh-handle)) new-term)
  )


(defmethod index-term-by-string (mesh-handle new-term string)
  (setf (gethash string (term-string-index mesh-handle)) new-term)
  )

(defmethod index-term-by-tree-number (mesh-handle new-term tree-number)
  (setf (gethash tree-number (term-tree-number-index mesh-handle)) new-term)
  )

(capi:define-interface mesh-tree-view ()
  () 
  (:panes
   (mesh-tree
    capi:tree-view
    :roots (godb::get-mesh-roots)
    :accessor assoc-tree
    :children-function (lambda (meshterm) (godb::child-nodes *current-mesh-handle* meshterm))
    :print-function (lambda (meshterm) (mesh-string  meshterm))
    :visible-min-width 200
    :visible-max-width nil
    :visible-min-height 200
    :visible-max-height nil
    :retain-expanded-nodes t
    :callback-type :interface-item
    :horizontal-scroll t
    :vertical-scroll t
    ))
  (:layouts
   (default-layout
    capi:simple-layout
    '(mesh-tree)))
  (:default-initargs
   :title "Mesh tree"))


(defun view-mesh ()
  (capi:display (make-instance 'mesh-tree-view)))
