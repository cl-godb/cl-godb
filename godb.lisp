;;; -*- Mode: Lisp -*-
(in-package "GODB")

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require "sql")
  (require "odbc"))


#.(sql:enable-sql-reader-syntax)

;;replaces Go-Apphandle


#| DEFCONSTANT causes a bug in SQL expressions.
(defconstant +rel-is-a+ 2)
(defconstant +rel-part-of+ 3)|#

(defvar +rel-is-a+ 2)
(defvar +rel-part-of+ 3)

(defclass o-handle ()
  ()
  )


(defclass go-handle (o-handle)
  (
   ;; GRAPH/DB maintainance.
   (term-accession-index :reader term-accession-index
                         :initform (make-hash-table :test #'eq))
   (term-id-index :reader term-id-index
                  :initform (make-hash-table :test #'eql))
   (term-name-index :reader term-name-index
                    :initform (make-hash-table :test #'equal))
   (association-accession-index :reader association-accession-index
                                :initform (make-hash-table :test #'eq))
   (association-gene-prod-index :reader association-gene-prod-index
                                :initform (make-hash-table :test #'eql))
   (gene-product-symbol-index :reader gene-product-symbol-index
                              :initform (make-hash-table :test #'equal))

   ;; Connection information.
   (connection :accessor connection :initform nil)
   )
  (:documentation "The GO Handle Class.
Class containing specifications for connecting to the database and
hash tables indexing current go terms."))


(defclass node ()
  ((is-a :accessor is-a
         :initarg :is-a)
   (part-of :accessor part-of
            :initarg :part-of)
   (children :accessor children
             :type list
             :initform ())
   (components :accessor components
               :type list
               :initform ())
   )
  (:default-initargs :is-a nil :part-of nil)
  )


(defclass term (node)
  ((term-id :accessor term-id :initarg :term-id)
   (name :reader term-name :initarg :term-name)
   (accession :accessor accession :initarg :accession)
   (term-type :accessor term-type :initarg :term-type)
   (definition :accessor definition :initarg :definition)
   (is-obsolete :accessor is-obsolete :initarg :is-obsolete :initform 0)
   (is-root :accessor is-root :initarg :is-root :initform 0))
  ) 


(defgeneric go-handle-p (x)
  (:method ((x go-handle)) t)
  (:method ((x t)) nil))

;;; Protocol.

(defgeneric connect (go-handle db-spec &optional connection)
  (:documentation "Sets up a connection to an underlying GO database
for the handle.
        arguments: go handle, connection string, optional connection
        usage: (godb:connect mygohandle ""data source name/user/password"")
        notes: Sets up a connection to an underlying GO database for the handle.
               If you have inserted your information in sqlcon, you can call connect as follows:
               (godb:connect mygohandle """" (godb:sqlcon))"))


(defgeneric get-term (go-handle go-id)
  (:documentation "Gets information for a go term using the go id.
        arguments: go handle, go id integer
        usage: (godb:get-term mygohandle 2145)
        returns: if term has been queried already, it simply returns the stored information.
                 else, it indexes the term and then returns its details"))

(defgeneric get-term-by-acc (go-handle go-acc)
  (:documentation "Gets information for a go term using its accession number.
        arguments: go handle, accession
        usage: (godb:get-term mygohandle ""GO:1248020"")
        returns: if term has been queried already, it simply returns the stored information.
                 else, it indexes the term and then returns its details"))

(defgeneric get-term-by-name (go-handle go-name)
  (:documentation "Gets information for a go term using its name.
        arguments: go handle, name string
        usage: (godb:get-term mygohandle ""Killing of Host Cells"")
        returns: if term has been queried already, it simply returns the stored information.
                 else, it indexes the term and then returns its details"))

(defgeneric get-name (go-handle go-id)
  (:documentation "Gets the name for a go term using its go-id.
        arguments: go handle, go id integer
        usage: (godb:get-term mygohandle 2145)
        returns: if term has been queried already, the name associated with the go id is returned.
                 else, the term is indexed and then the name is returned"))

(defgeneric has-synonym-p (go-handle go-id syn)
  (:documentation "Checks if a synonym exists
        arguments: go handle, go id, synonym
        usage: (godb:has-synonym mygohandle, 2145 ""proposed synonym"")
        returns: T if the proposed synonym is a synonym, NIL if not"))                           

(defgeneric index-term-by-id (go-handle new-term id)
  (:documentation "Adds a term to the term-id table, indexed by its go id     
        arguments: go handle, term (type term), go id
        usage: (godb:index-term-by-id mygohandle myterm 2145)
        returns:
        notes:  This is a function that is called when a new term is created.  It adds it to the
                hash table, indexed by its id.  This is not a function you'd normally call yourself."))

(defgeneric index-term-by-accession (go-handle new-term accession)
  (:documentation "Adds a term to the term-accession hash table, indexed by accession.
        arguments: go handle, term (type term), accession
        usage: (godb:index-term-by-accession mygohandle myterm ""GO:0000134"")
        returns:
        notes:  This is a function that is called when a new term is created.  It adds it to the
                hash table, indexed by its accession.  This is not a function you'd normally call yourself."))

(defgeneric index-term-by-name (go-handle new-term name)
  (:documentation "Adds a term to the term-name hash table, indexed by its name.
        arguments: go handle, term (type term), name string
        usage: (godb:index-term-by-accession mygohandle myterm ""Killing of Host Cells"")
        returns:
        notes:  This is a function that is called when a new term is created.  It adds it to the
                hash table, indexed by its name.  This is not a function you'd normally call yourself."))

(defgeneric is-term-id-indexed-p (gh go-id)
  (:documentation "Checks to see if the input go-id is currently indexed.
        arguments: go handle, go-id
        usage: (godb:is-id-indexed mygohandle 2145)
        returns: Object of type term if it's indexed, otherwise returns nothing"))

(defgeneric is-term-accession-indexed-p (gh go-acc)
  (:documentation "Checks to see if the input accession number is currently indexed.
        arguments: go handle, accession
        usage: (godb:is-accession-indexed mygohandle ""GO:0000124"")
        returns: Object of type term if it's indexed, otherwise returns nothing"))

(defgeneric is-term-name-indexed-p (gh go-name)
  (:documentation "Checks to see if the input name is currently indexed.
        arguments: go handle, name
        usage: (godb:is-name-indexed mygohandle ""Killing of Host Cells"")
        returns: Object of type term if it's indexed, otherwise returns nothing"))


(defun sqlcon ()
  "Connects to sql using stored connection information."
  (sql:connect "GODB_internal/root" :database-type :odbc :if-exists :old))


(defvar *current-godb-handle* nil)

(defun make-godb-handle ()
  "Creates a new instance of the go handle class and assigns
it to the current-godb-handle variable"
  (setf *current-godb-handle* (make-instance 'go-handle)))




(defmethod is-term-id-indexed-p ((gh go-handle) (go-id integer))
  (multiple-value-bind (value found) 
      (gethash go-id (term-id-index gh))
    (when found value)
    )
  )

(defmethod is-term-accession-indexed-p ((gh go-handle) (go-acc symbol))
  (multiple-value-bind (value found)
      (gethash go-acc (term-accession-index gh))
    (when found value)
    )
  )


(defmethod is-term-accession-indexed-p ((gh go-handle) (go-acc string))
  (multiple-value-bind (value found)
      (gethash (intern go-acc "GODB") (term-accession-index gh))
    (when found value)
    )
  )


(defmethod is-term-name-indexed-p ((gh go-handle) go-name)
  (multiple-value-bind (value found) 
      (gethash go-name (term-name-index gh))
    (when found value)
    )
  )

(defmethod connect ((gh go-handle) (db-spec string) &optional connection)
  (format t "~S connecting as ~S~%" gh db-spec)
  (if connection
      (setf (connection gh) connection)
      (setf (connection gh) (sql:connect db-spec))))


(defun create-term (id name term-type accession is-obsolete is-root)
"This function makes a new instance of the term class given input for its slots"
  (make-instance 'term
                 :term-name name
                 :term-id id
                 :term-type term-type
                 :accession accession
                 :is-obsolete is-obsolete
                 :is-root is-root
                 )
  )

(defmethod get-term ((go-handle go-handle) (go-id integer))
  ;; Step 1.
  ;; See if we already have it in the GO-HANDLE.
  (let ((index (is-term-id-indexed-p go-handle go-id)))
    (if index
        index
        ;; Step 2.
        ;; query db, get info for term, make instance with that data
        (destructuring-bind (id name term-type acc is-obsolete is-root)
            (first (sql:select [id] [name] [term_type] [acc] [is_obsolete] [is_root]
                               :from [|term|]
                               :where [= go-id [id]]
                               :database (connection go-handle)
                               ))
          (when id
              (let* ((acc (intern acc "GODB"))
                     (new-term (create-term id name term-type acc
                                            is-obsolete is-root)))
                (index-term-by-id go-handle new-term id)
                (index-term-by-name go-handle new-term name)
                (index-term-by-accession go-handle new-term acc)
                new-term
                ))))))


(defmethod get-name ((go-handle go-handle) (go-id integer))
 
  ;;select returns two lists, one of values, followed by one of column names
  ;;this prints the first value of the list of values 
    
  ;;check hashtable
  (let ((index (is-term-id-indexed-p go-handle go-id)))
    (if index
        (term-name index)
        ;;if not in table, query db
        (destructuring-bind (id name term-type acc is-obsolete is-root)
            (first (sql:select [id] [name] [term_type] [acc] [is_obsolete] [is_root]
                               :from [|term|]
                               :where [= go-id [id]]
                               :database (connection go-handle)
                               ))
          (when id
              (let* ((acc (intern acc "GODB"))
                     (new-term (create-term id name term-type acc
                                            is-obsolete is-root)))
                (index-term-by-id go-handle new-term id)
                (index-term-by-name go-handle new-term name)
                (index-term-by-accession go-handle new-term acc)
                name
                ))))))

(defmethod get-name ((go-handle go-handle) (go-acc string))
 
  ;;select returns two lists, one of values, followed by one of column names
  ;;this prints the first value of the list of values 
    
  ;;check hashtable
  (let ((index (is-term-accession-indexed-p go-handle go-acc)))
    (if index
        (term-name index)
        ;;if not in table, query db
        (destructuring-bind (id name term-type acc is-obsolete is-root)
            (first (sql:select [id] [name] [term_type] [acc] [is_obsolete] [is_root]
                               :from [|term|]
                               :where [= go-acc [acc]]
                               :database (connection go-handle)
                               ))
          (when id
              (let* ((acc (intern acc "GODB"))
                     (new-term (create-term id name term-type acc
                                            is-obsolete is-root)))
                (index-term-by-id go-handle new-term id)
                (index-term-by-name go-handle new-term name)
                (index-term-by-accession go-handle new-term acc)
                name
                ))))))


(defmethod get-term-by-acc ((go-handle go-handle) (go-acc symbol))
  (get-term-by-acc go-handle (string go-acc)))


(defmethod get-term-by-acc ((go-handle go-handle) (go-acc string))

  ;;check hash table indexed by accession  
  ;;if not in table, query db, add to table
  (let ((index (is-term-accession-indexed-p go-handle go-acc)))
    (if index
        index
        (destructuring-bind (id name term-type acc is-obsolete is-root)
            (first (sql:select [id] [name] [term_type] [acc] [is_obsolete] [is_root]
                               :from [|term|]
                               :where [= go-acc [acc]]
                               :database (connection go-handle)
                               ))
          (when id
              (let* ((acc (intern acc "GODB"))
                     (new-term (create-term id name term-type acc
                                            is-obsolete is-root)))
                (index-term-by-id go-handle new-term id)
                (index-term-by-name go-handle new-term name)
                (index-term-by-accession go-handle new-term acc)
                new-term
                ))))))


(defmethod get-term-by-name ((go-handle go-handle) go-name)
  (let ((index (is-term-name-indexed-p go-handle go-name)))
    (if index
        index
        (destructuring-bind (id name term-type acc is-obsolete is-root)
            (first (sql:select [id] [name] [term_type] [acc] [is_obsolete] [is_root]
                               :from [|term|]
                               :where [= go-name [name]]
                               ))
          (when id
              (let* ((acc (intern acc "GODB"))
                     (new-term (create-term id name term-type acc
                                            is-obsolete is-root)))
                (index-term-by-id go-handle new-term id)
                (index-term-by-name go-handle new-term name)
                (index-term-by-accession go-handle new-term acc)
                new-term
                ))))))

(defmethod has-synonym-p (go-handle go-id syn)
  (let ((synonyms (sql:select [term_synonym.term_synonym]
                              :from [|term_synonym|]
                              :where [and [= [term_synonym.term_id] go-id]
                              [= [term_synonym.term_synonym] syn]])))
    (if synonyms T NIL)))


(defmethod index-term-by-id (go-handle new-term id)
  (setf (gethash id (term-id-index go-handle)) new-term) 
  )

(defmethod index-term-by-accession (go-handle new-term accession)
  (setf (gethash accession (term-accession-index go-handle)) new-term)
  )

(defmethod index-term-by-name (go-handle new-term name)
  (setf (gethash name (term-name-index go-handle)) new-term)
  )


(defun get-root-terms ()
  "Returns all root terms."
  (first (sql:select [*] :from [|term|] :where [= [is_root] 1])))

#.(sql:disable-sql-reader-syntax)

;;; end of file -- godb.lisp --
