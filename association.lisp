;;; -*- Mode: Lisp -*-
(in-package "GODB")

(eval-when (:compile-toplevel :load-toplevel :execute)
  (require "sql")
  (require "odbc"))

#.(sql:enable-sql-reader-syntax)


(defclass association ()
  ((id :accessor assoc-id :initarg :assoc-id)
   (term-id :accessor assoc-term-id :initarg :assoc-term-id)
   (gene-product-id :accessor gene-product-id :initarg :gene-product-id)
   (is-not :accessor assoc-is-not :initarg :is-not)
   (role-group :accessor assoc-role-group :initarg :assoc-role-group)
   (acc :accessor assoc-acc :initarg :assoc-acc)
   )
(:documentation "The Association  Class.
This class is a representation of the association table in the go database, with the addition
of a slot for the accession."))
  )

(defgeneric association-list (gh go-acc)
  (:documentation "Gets associations for a given accession number.
        arguments: go handle, accession
        usage: (godb:association-list mygohandle ""GO:0000124"")
        returns: list of objects of type association that match the term id for the 
                 supplied accession number
        notes: Creates objects of type 'association' and then indexes them in the hash
               on both accession and gene-product-id"))

(defgeneric associations-by-gene-prod (gh gene-prod-id)
  (:documentation "Gets associations for a given gene product id.
        arguments: go handle, gene product id
        usage: (godb:get-association-list mygohandle 16720)
        returns: list of objects of type association that match the supplied gene product id
        notes: Creates objects of type 'association' and then indexes them in the hash
               on both accession and gene-product-id"))

(defgeneric is-association-accession-indexed-p (gh accession)
  (:documentation "Checks whether a given accession number is presently indexed.
        arguments: go handle, accession number
        usage: (godb:is-association-accession-indexed mygohandle ""GO:0000124"")
        returns: Value stored in hash if it's indexed, or nothing if it's not
        notes: This is a function mostly for use by other functions.  It checks
               whether or not a given accession number has been stored in the
               association-accession index"))

(defgeneric is-association-gene-prod-indexed-p (gh gene-prod-id)
  (:documentation "Checks whether a given gene product id is presently indexed.
        arguments: go handle, gene product id
        usage: (godb:is-association-gene-prod-indexed mygohandle 124)
        returns: Value stored in hash if it's indexed, or nothing if it's not
        notes: This is a function mostly for use by other functions.  It checks
               whether or not a given gene-product-id has been stored in the
               association-gene-product index"))

(defgeneric index-association-by-accession (gh new-association accession)
  (:documentation "Adds the given association to the hash table, indexed by its accession number.
        arguments: go handle, association (i.e. of type association), accession number
        usage: (godb:index-association-by-accession mygohandle myassociation ""GO:0000124"")
        returns: -"))

(defgeneric index-association-by-gene-prod (gh new-association gene-prod-id)
  (:documentation "Adds the given association to the hash table, indexed by its gene product
        arguments: go handle, association (i.e. of type association), gene product id
        usage: (godb:index-association-by-gene-prod mygohandle myassociation 124)
        returns: -"))


(defun create-association (id term-id gene-product-id is-not role-group acc)
"Makes a new object of type association, given the inputs for the slots"
  (make-instance 'association
                 :assoc-id id
                 :assoc-term-id term-id
                 :gene-product-id gene-product-id
                 :is-not is-not
                 :assoc-role-group role-group
                 :assoc-acc acc
                 ))

(defmethod association-list ((gh go-handle) go-acc)
  (let ((index (is-association-accession-indexed-p gh go-acc)))
    (if index
        index
        (let ((associations (sql:select [association.id] [association.term_id] [association.gene_product_id] 
                                        [association.is_not] [association.role_group] [term.acc]
                                        :from '([term] [association])
                                        :where [and [= go-acc [term.acc]]
                                        [= [term.id] [association.term_id]]]
                                        :database (connection gh))))
          (when associations
            (let* ((assoc-list nil) (go-acc (intern go-acc "GODB")))
              (dolist (x associations)
                (let ((new-assoc (make-instance 'association
                                                :assoc-id (first x)
                                                :assoc-term-id (nth 1 x)
                                                :gene-product-id (nth 2 x)
                                                :is-not (nth 3 x)
                                                :assoc-role-group (nth 4 x)
                                                :assoc-acc (nth 5 x))))
                  (index-association-by-gene-prod gh new-assoc (gene-product-id new-assoc)) 
                  (push new-assoc assoc-list)))
              (index-association-by-accession gh assoc-list go-acc)
              assoc-list))))))


(defmethod associations-by-gene-prod ((gh go-handle) (gene-prod-id integer))
  (let ((index (is-association-gene-prod-indexed-p gh gene-prod-id)))
    (if index
        index
        (let ((associations (sql:select [association.id] [association.term_id] [association.gene_product_id] 
                                        [association.is_not] [association.role_group] [term.acc]
                                        :from '([term] [association])
                                        :where [and [= gene-prod-id [association.gene_product_id]]
                                        [= [term.id] [association.term_id]]]
                                        :database (connection gh)
                                        :distinct t)))
          (when associations
            (let* ((assoc-list nil))
              (dolist (x associations)
                (let ((new-assoc (make-instance 'association
                                                :assoc-id (first x)
                                                :assoc-term-id (second x)
                                                :gene-product-id (third x)
                                                :is-not (fourth x)
                                                :assoc-role-group (fifth x)
                                                :assoc-acc (intern (sixth x) "GODB"))))
                  (index-association-by-accession gh new-assoc (assoc-acc new-assoc))
                  (push new-assoc assoc-list)))
              (index-association-by-gene-prod gh assoc-list gene-prod-id)
              assoc-list))))))

(defmethod is-association-accession-indexed-p ((gh go-handle) (accession symbol))
  (multiple-value-bind (value found)
      (gethash accession (association-accession-index gh))
    (when found value))
  )

(defmethod is-association-accession-indexed-p ((gh go-handle) (accession string))
  (multiple-value-bind (value found)
      (gethash (intern accession "GODB") (association-accession-index gh))
    (when found value))
  )

(defmethod is-association-gene-prod-indexed-p ((gh go-handle) gene-prod-id)
  (multiple-value-bind (value found)
      (gethash gene-prod-id (association-gene-prod-index gh))
    (when found value))
  )


(defmethod index-association-by-accession ((gh go-handle) new-association accession)
  (setf (gethash accession (association-accession-index gh)) new-association)
  )

(defmethod index-association-by-gene-prod ((gh go-handle) new-association gene-prod-id)
  (setf (gethash gene-prod-id (association-gene-prod-index gh)) new-association)
  )



#.(sql:disable-sql-reader-syntax)

;;; end of file -- association.lisp --
